'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "News",
      [
        {
          title: "titulo de prueba",
          content:"contenido de prueba",
          image:
            "https://www.designevo.com/res/templates/thumb_small/colorful-hand-and-warm-community.png",
          type: "tipo de prueba",
          category: "news",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          title: "titulo de prueba 2",
          content:"contenido de prueba 2",
          image:
            "https://www.designevo.com/res/templates/thumb_small/colorful-hand-and-warm-community.png",
          type: "tipo de prueba 2",
          category: "news",
          createdAt: new Date(),
          updatedAt: new Date(),
        }
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
