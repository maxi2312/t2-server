'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "Roles",
      [
        {
          name: 'admin',
          description: "administrator",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'user',
          description: "user",
          createdAt: new Date(),
          updatedAt: new Date(),
        }
      ],
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
