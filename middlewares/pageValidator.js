const { check } = require("express-validator");

const updateFields = [
    check('content').notEmpty().withMessage('Content is required.')
];

module.exports = {
    updateFields
};