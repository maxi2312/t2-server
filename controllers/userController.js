const models = require("../models/index");
const { validationResult } = require("express-validator");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const config = require("../config/config");
const User = models.User;

const create = async (req, res) => {
  console.log(req);
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  try {
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const password = req.body.password;
    const hashPassword = bcrypt.hashSync(password, 10);
    // roleId: 1 is for administrator user
    // roleId: 2 is for default user
    const newUser = await User.create({
      firstName,
      lastName,
      email,
      password: hashPassword,
      roleId: 2,
    });
    const token = jwt.sign({ id: newUser.id }, config.secret);
    res.status(200);
    res.set({
      "Content-type": "application/json",
      Authorization: "bearer " + token,
    });
    res.send({
      firstName: newUser.firstName,
      lastName: newUser.lastName,
      email: newUser.email,
      roleId: newUser.roleId,
    });
  } catch (error) {
    console.log(error);
    res.status(400).json({ msg: "Connection error to create a new user" });
  }
};

const data = async (req, res, next) => {
  const { id } = req.decoded;
  try {
    const user = await User.findOne({ where: { id: id } });
    if (user === null) {
      res.status(403);
      res.set({
        "Content-type": "application/json",
      });
      res.send({ error: `The ID is not valid.` });
    } else {
      const userData = {
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        roleId: user.roleId,
        organizationId: user.organizationId,
        deletedAt: user.deletedAt,
        createdAt: user.createdAt,
        updatedAt: user.updatedAt,
      };
      res.status(200);
      res.set({
        "Content-type": "application/json",
      });
      res.send(userData);
    }
  } catch (error) {
    res.status(403);
    res.set({
      "Content-type": "application/json",
    });
    res.send({ error: `Can't connect with database.` });
  }
};

const edit = async (req, res, next) => {
  const { id } = req.decoded;
  const { firstName, lastName } = req.body;
  try {
    const update = await User.update(
      { firstName: firstName, lastName: lastName },
      { where: { id: id } }
    );
    if (update[0] === 0) {
      res.status(403);
      res.set({
        "Content-type": "application/json",
      });
      res.send({ error: `The ID is not valid.` });
    } else {
      const user = await User.findOne({ where: { id: id } });
      res.status(200);
      res.set({
        "Content-type": "application/json",
      });
      res.send({
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
      });
    }
  } catch (error) {
    res.status(403);
    res.set({
      "Content-type": "application/json",
    });
    res.send({ error: `Can't connect with database.` });
  }
};

module.exports = {
  data,
  create,
  edit,
};
