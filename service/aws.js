const AWS = require('aws-sdk')
require('dotenv').config()

const BUCKET_NAME = 'alkemy-ong'
const folder = 'grupo2'
const s3 = new AWS.S3({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  signatureVersion: 'v4'
})

const uploadFile = async (fileName, fileStream) => {
  const params = {
    Bucket: BUCKET_NAME,
    Key: `${folder}/${Date.now()}-${fileName}`,
    Body: fileStream
  }
  try {
    const response = await s3.upload(params).promise()
    return response
  } catch(error) {
    throw {message: error.message}
  } 
}

module.exports = {uploadFile}