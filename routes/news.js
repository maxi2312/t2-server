const express = require("express");
const router = express.Router();
const newsController = require("../controllers/newsController");
const tokenValidator = require("../middlewares/tokenValidator");
const newValidator = require("../middlewares/newValidator");
const multer = require('multer');
const storage = multer.memoryStorage();
const upload = multer({
  storage: storage,
  limits: {
    fields: 4,
    fieldSize: 2*2048*2048,
    fileSize: 60000000,
    files: 1,
    parts: 5
  }
});

router.post(
  "/",
  tokenValidator.validate,
  upload.single('image'),
  newValidator.creation,
  newsController.create
);
router.get("/:id", newsController.detail);
router.delete(
  "/:id", 
  tokenValidator.validate, 
  newsController.deleteNew);
router.get(
  "/",
  newsController.allNews
);
router.patch(
  "/:id",
  tokenValidator.validate,
  upload.single('image'),
  newValidator.edition,
  newsController.editNews
);

router.post(
  '/ckeditor',
  upload.single('upload'),
  newsController.uploadImage
)


module.exports = router;
