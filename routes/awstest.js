const router = require('express').Router()
const aws = require('../service/aws')
const multer = require('multer');
const storage = multer.memoryStorage();
const upload = multer({
  storage: storage,
  limits: {
    fields: 4,
    fileSize: 60000000,
    files: 1,
    parts: 5
  }
});
//No separo en controlador para que lo puedan ver mas rapido

router.post('/', async (req, res) => {
  upload.single('image')(req, res, async (err) => { 
    console.log(req.file)
    const result = await aws.uploadFile(req.file.originalname, req.file.buffer)
    res.json(result)
    //const result = await aws.uploadFile(req.file)
  })
})


module.exports = router