var express = require('express');
var router = express.Router();
const token = require('../middlewares/tokenValidator');
const userValidator = require('../middlewares/userValidator');
const userController = require('../controllers/userController');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/:id', token.validate, userController.data);

router.patch('/:id', token.validate, userValidator.updateFields, userController.edit);

module.exports = router;
