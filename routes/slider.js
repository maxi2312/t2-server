const express = require("express");
const router = express.Router();
const sliderController = require("../controllers/sliderController");
const tokenValidator = require("../middlewares/tokenValidator");
const sliderValidation = require("../middlewares/sliderValidation");
const multer = require('multer');
const storage = multer.memoryStorage();
const upload = multer({
  storage: storage,
  limits: {
    fields:3 ,
    fileSize: 60000000,
    files: 1, 
    parts: 4
  }
});

router.post(
  "/",
  tokenValidator.validate,
  upload.single('image'),
  sliderValidation.creation,
  sliderController.create
);
router.get("/", sliderController.slidersList);
router.delete("/:id", tokenValidator.validate, sliderController.deleteSlider);
module.exports = router;
