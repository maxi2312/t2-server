var express = require('express');
var router = express.Router();
var ong = require('../controllers/ongController');
const multer = require('multer');
const tokenValidator = require('../middlewares/tokenValidator');
const ongValidator = require('../middlewares/ongValidator');
const ongController = require('../controllers/ongController');
const storage = multer.memoryStorage();
const upload = multer({
  storage: storage,
  limits: {
    fields: 9,
    fileSize: 60000000,
    files: 1,
    parts: 10
  }
});

/* GET Info Organization */
router.get('/', ong.findAll);

router.patch("/:id", tokenValidator.validate, upload.single('image'), ongValidator.updateFields, ongController.edit);

module.exports = router;


